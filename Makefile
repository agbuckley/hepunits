
.PHONY: all clean

all: hepunits.tar.gz
	@true

hepunits.tar.gz: README ChangeLog hepunits.pdf hepunits.sty hepunits.tex
	@mkdir hepunits
	@cp $^ hepunits
	@tar czf $@ hepunits
	@rm -r hepunits

hepunits.pdf: hepunits.tex
	@(rm -f hepunits.{aux,toc,lof,lot} && pdflatex hepunits.tex && pdflatex hepunits.tex && rm -f hepunits.{aux,toc,lof,lot})

clean:
	@rm -f rm *.aux *.toc *.lof *.lot *.log *.out *.tar.gz *.pdf
